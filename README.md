Bandstat [![Build Status](https://travis-ci.org/matee/bandstat.png)](https://travis-ci.org/matee/bandstat)
========

Track which songs your band plays, make nice statistics, and check when other bands play your songs.

What?
-----
TODO

How?
----
TODO

Why?
----

My band plays worship regularly in our church for 8 years. We usually don't do playlists, and even when we do, we still tend to change a few songs as we go. Sometimes we realize we tend to play some songs more often than the others, which leads to some kind of burnout. For the congregation and for us, too. Then we want to play some songs we haven't been playing for a while, but we can't remember. Mainly because it has been really long since we last played those. 


Wish list
---------

- ability to use some official music authors database (like SOZA for Slovakia)
- generate official announcements for the officials (again SOZA)
