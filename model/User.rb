class User
  include DataMapper::Resource
  
  property  :id,        Serial
  property  :name,      String
  property  :email,     String
  property  :pass_hash, String
  
  property  :created_at,  DateTime
  property  :updated_at,  DateTime

  has n, :authors

end