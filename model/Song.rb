class Song
  include DataMapper::Resource
  
  property  :id,            Serial
  property  :title,         String
  property  :title_alt,     String
  property  :genre,         String
  property  :form,          String
  property  :band_type,     String
  
  property  :created_at,  DateTime
  property  :created_by,  String
  property  :updated_at,  DateTime
  property  :updated_by,  String
  
  has n, :authorships
  has n, :authors, :through => :authorships
  
end