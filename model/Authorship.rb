class Authorship
  include DataMapper::Resource
  
  property :id,         Serial
  property :created_at, DateTime
  
  belongs_to :author
  belongs_to :song
  
end