class Author
  include DataMapper::Resource
  
  property  :id,          Serial
  property  :first_name,  String
  property  :last_name,   String
  property  :pseudonym,   String
  property  :birth_no,    String  # rodne cislo in Slovak / this might better be Integer
  property  :birth_date,  DateTime
  property  :address,     Text
  property  :zip,         Integer
  property  :phone,       String
  property  :email,       String
  property  :bank_acc_no, Integer
  property  :bank_code,   Integer
  
  property  :created_at,  DateTime
  property  :created_by,  String
  property  :updated_at,  DateTime
  property  :updated_by,  String
  
  belongs_to :user

  has n, :authorships
  has n, :songs, :through => :authorships

end