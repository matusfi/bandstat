require 'sinatra'
# require 'haml'
require 'dm-core'
require 'dm-migrations'

# Haml::Template.options[:ugly] = true

## Show logs from DataMapper
# DataMapper::Logger.new($stdout, :debug)

# DataMapper.setup(:default, "sqlite://#{Dir.pwd}/db/dev.db")
DataMapper.setup(:default, 'sqlite://:memory:')

Dir[File.join(File.dirname(__FILE__), 'model', '*.rb')].each do |model|
  require_relative model
end

DataMapper.finalize

## Uncomment for persistent DB
# DataMapper.auto_upgrade!

get '/' do
  'Hey everybody'
end
